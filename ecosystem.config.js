module.exports = {
    apps : [{
        name: 'HOTS Heros Picker',
        script: '/opt/hots-heros-picker_node/dist/server.js',
        instances: 1,
        autorestart: true,
        watch: true,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'production'
        },
    }],

    deploy : {
        production : {
            user : 'gitlab',
            host : 'api.hotsdraft.jasonpoindexter.io',
            ref  : 'origin/master',
            repo : 'git@gitlab.com:jsonpoindexter/hots-heros-picker_node.git',
            path : '/opt/hots-heros-picker_node',
            'post-deploy' : 'npm install && pm2 reload /opt/hots-heros-picker_node/ecosystem.config.js --env production'
        }
    }
};
